import { DataInvalidaException } from './DataInvalidaException.js';
export class DateConverter {

    constructor() {
        throw new Error('Essa classe não pode ser intanciada pelo operador \"new\"');
    }

    static paraData(dataTexto) {
        //                      /\d{2}\/\d{2}\/\d{4}/
        let regExp = new RegExp(/\d{2}\/\d{2}\/\d{4}/);
        if(!regExp.test(dataTexto)) {
            throw new DataInvalidaException();
        }
        return new Date(...dataTexto.split("/").reverse().map((item, index) => item - (index % 2))) 
    }

    static paraTexto(data) {
        if(!data instanceof Date) {
            throw new Error('Insira um objeto do tipo \'new Date\' ');
            return;
        }

        let diaData = data.getDate();
        let mesData = data.getMonth() + 1;
        let anoData = data.getFullYear();

        return `${diaData}/${mesData > 0 && mesData < 10 ? "0"+mesData : mesData}/${anoData}`.trim();
    }

    static ofPattern(pattern, data) {

        let reverse = DateConverter._isReverse(pattern);
        let separator =  reverse ? 
            DateConverter._getSeparator(pattern, 4):
            DateConverter._getSeparator(pattern, 2);            

        let dataArr = [data.getFullYear(),
                data.getMonth(),
                data.getDate()]   
        return reverse ?
            DateConverter._mountDateFormat(dataArr, separator) :
            DateConverter._mountDateFormat(dataArr.reverse(), separator);
    }


    /**
     * Métodos do POJO
     */

    static _mountDateFormat(arr, separator) {
        if(DateConverter._verifyDigitsOfMonth(arr[1] + 1)) {
            arr[1] = `0${arr[1]}`; 
        }
        return `${arr[0]}${separator}${arr[1] + 1}${separator}${arr[2]}`;
    }


    static _isReverse(pattern) {
        const regExp = new RegExp(/[a-z]{4}/);
        const reverseStr = pattern.substr(0,4);

        return regExp.test(reverseStr)  ? true : false;
    }

    static _getSeparator(pattern, index) {
        return pattern.substr(index, 1);
    }


    static _verifyDigitsOfMonth(month) {
        return month > 0 && month < 10;
    }


}