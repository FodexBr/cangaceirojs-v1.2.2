export class View {

    constructor(elemento) {
        this._elemento = elemento;
    }

    template(model) {
        throw new Error('O método template deve ser implementado por classes filhas!');
    }

    update(model) {
        this._elemento.innerHTML = this.template(model);
    }


}