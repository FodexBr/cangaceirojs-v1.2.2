import { View } from './View.js';
export class MensagemView extends View {

    constructor(elemento) {
        super(elemento);
    }

    template(model) {
        return model.texto.length ? `<p	class="alert alert-info">${model.texto}</p>` : '';
    }


}