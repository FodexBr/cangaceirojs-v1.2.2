import { Negociacao, NegociacaoDAO, Negociacoes} from '../domain/index.js';
import {     
        DateConverter, 
        Mensagem, 
        MensagemView, 
        NegociacoesView,
        DataInvalidaException,
        View
    } from '../ui/index.js';

import {    
        ApplicationException, 
        Bind, 
        getExceptionMessage,
        debounce,
        bindEvent,
        controller,
        ConnectionFactory, 
        getNegociacaoDao, 
        HttpService, 
        InvalidArgumentsExcepion, 
        ProxyFactory    
    } from '../util/index.js';

@controller("#data", "#quantidade", "#valor")
export class NegociacaoController {
    constructor(_inputData, _inputQuantidade, _inputValor) {

        const self = this;
        const $ = document.querySelector.bind(document);
        
        this._inputData = _inputData;
        this._inputData.value = DateConverter.paraTexto(new Date());

        this._inputQuantidade = _inputQuantidade;
        this._inputValor = _inputValor;

        // this._negociacoesService = new NegociacaoService();
        this._negociacoes = new Bind(
            new Negociacoes(),
            new NegociacoesView($("#negociacoes")),
            'adiciona', 'esvazia');

        this._mensagem = new Bind(
            new Mensagem(), 
            new MensagemView($("#mensagemView")), 
            'texto');
        
            this._init();
    }

    // codigos assincronos com async/await
    // OBS: o método deve comevar com "async"
    // Dentro do método usamos "await" em algum método que retorna uma promise

    async _init() {

        try {
            const dao = await getNegociacaoDao();
            const negociacoes = await dao.listaTodos();

            negociacoes.forEach(n => this._negociacoes.adiciona(n));
            this._mensagem.texto = 'As negociações foram buscadas com sucesso!';

        }catch(err) {
            this._mensagem.texto = getExceptionMessage(err);
        }
    }

    _initInputs(){
        this._inputData.value = DateConverter.paraTexto(new Date());
    }

    @bindEvent('click', '#botao-importa')//adicionando o metadado BindEvent aos metodos importaNegociacoes, adiciona, apaga
    @debounce()
    async importaNegociacoes() {
        try {
            const { NegociacaoService } = await import("../domain/negociacao/NegociacaoService.js");
            const service = new NegociacaoService();

            let negociacoes = await service.obterNegociacoesDoPeriodo();
            negociacoes = negociacoes
            .filter(novaNegociacao => 
                !this._negociacoes
                    .negociacoes
                    .some(negociacaoExistente => novaNegociacao.equals(negociacaoExistente))); 

            negociacoes.forEach(negociacao => this._negociacoes.adiciona(negociacao));
            
            if(negociacoes.length > 0) 
                this._mensagem.texto = 'Negociações do período importadas com sucesso!';
            else 
                this._mensagem.texto = 'As negociações do período já foram importadas!';

        }catch(err) {
            this._mensagem.texto = getExceptionMessage(err);
        }
    }

    @bindEvent('submit', '.form')
    async adiciona(event) {
        
        try {
            event.preventDefault();
            const negociacao = this._criaNegociacao();
            
            const dao = await getNegociacaoDao();
            await dao.adiciona(negociacao);

            this._negociacoes.adiciona(negociacao);
            this._mensagem.texto = 'Negociação adicionada com sucesso!';
            this._limpar(event.target);

        }catch(err) {
            console.log(err);
            console.log(err.stack);

            if(err instanceof DataInvalidaException) 
                this._mensagem.texto = getExceptionMessage(err);
            else 
                this._mensagem.texto = getExceptionMessage(err);
        }

    }

    @bindEvent('click', '#botao-apaga')
    async apaga() {

        try {
            const dao = await getNegociacaoDao();
            await dao.apaga();

            this._negociacoes.esvazia();
            this._mensagem.texto = 'As negociações foram apagadas com sucesso!';

        }catch(err) {
            this._mensagem.texto = getExceptionMessage(err);
        }
    }

    _criaNegociacao() {
        //let $ = document.querySelector.bind(document);

        return new Negociacao({
            "_data": DateConverter.paraData(this._inputData.value),
            "_quantidade" : parseInt(this._inputQuantidade.value),
            "_valor": parseFloat(this._inputValor.value)
        });
    }

    _limpar(target) {
        target.reset();
        this._inputQuantidade.value = 1;
        this._inputValor.value = 0.0;
        
        this._inputData.focus();
        this._initInputs();
    }
}