import { Negociacao } from './Negociacao.js';
import { InvalidArgumentsExcepion } from '../../util/InvalidArgumentsException.js';

export class NegociacaoDAO {

    constructor(connection) {
        this._connection = connection;
        this._store = "negociacoes";
    }


    adiciona(negociacao) {
        if(!negociacao instanceof Negociacao) {
            throw new InvalidArgumentsException("O parâmetro passado não é uma instância de Negociacao");
        }

        return new Promise((resolve, reject) => {

            const store = this._getStoreTransactional();
            const request = store.add(negociacao);

            request.onsuccess = event => {
                resolve();
            }

            request.onerror = event => {
                console.log(event.target.error);
                reject("Não foi possível salvar a negocação");
            }

        });
    }

    listaTodos() {
        return new Promise((resolve, reject) => {
            const negociacoes = [];
            const store = this._getStoreTransactional();
            const cursor = store.openCursor();

            cursor.onsuccess = event => {
               const atual = event.target.result;

               if(atual) {
                    const obj = atual.value;
                    negociacoes.push(new Negociacao({
                        "_data" : obj._data,
                        "_quantidade" : obj._quantidade,
                        "_valor": obj._valor
                    }));

                    atual.continue();
               }else {
                   resolve(negociacoes);
               }
            }

            cursor.onerror = event => {
                console.error(event.target.error);
                reject('Não foi possível buscar as negociações!');
            }
        });
    }

    apaga() {
        return new Promise((resolve, reject) => {

            const store = this._getStoreTransactional();
            const request = store.clear();

            request.onsuccess = event => resolve();
            request.onerror = event => {
                console.error(event.target.error)
                reject('Não foi possível apagar as negociaçoes cadastradas no banco.');
            } 
        });
    }

    //Métodos auxiliares para obtenção de uma store transacional
    _getStoreTransactional() {
        const transaction = this._getTransaction();
        return transaction.objectStore(this._store);
    }

    _getTransaction(mode = 'readwrite') {
        return this._connection.transaction([this._store], mode);
    }

  
}