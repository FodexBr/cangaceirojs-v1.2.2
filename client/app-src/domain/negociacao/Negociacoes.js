import { Negociacao } from './Negociacao.js';
export class Negociacoes {

    constructor() {
        this._negociacoes = [];
        Object.freeze(this);
    }


    adiciona(negociacao) {
        this._negociacoes.push(negociacao);
    }

    esvazia() {
        this._negociacoes.length = 0;
    }

    get negociacoes() { return [].concat(this._negociacoes); }

    get total() {
        return this._negociacoes.reduce((total, n) => total + n.volume, 0.0);
    }

 

}