import { HttpService } from '../../util/HttpService.js';
import { Negociacao } from './Negociacao.js';
import { ApplicationException } from '../../util/ApplicationException.js';

export class NegociacaoService {

    constructor() {
        this._http = new HttpService();
    }

    _obterNegociacoesDaSemana() {
        return this._buscaNegociacoes('http://localhost:3000/negociacoes/semana')
        .catch(err => {
            throw new ApplicationException('Não foi possível buscas as negociacoes da semana!');
        });
    }

    _obterNegociacoesDaSemanaAnterior() {
        return this._buscaNegociacoes('http://localhost:3000/negociacoes/anterior')
        .catch(err => {
            throw new ApplicationException('Não foi possível buscas as negociacoes da semana anterior!');
        });
    }

    _obterNegociacoesDaSemanaRetrasada() {
        return this._buscaNegociacoes('http://localhost:3000/negociacoes/retrasada')
        .catch(err => {
            throw new ApplicationException('Não foi possível buscas as negociacoes da semana retrasada!');
        });
    }

    async obterNegociacoesDoPeriodo() {
       /** Método Sort para Ordernar arrays comparando criterios (Igual ao Java)
         *  0 => iguais
         *  1 => o arg0 > arg1
         *  -1 => o arg0 < arg1
         */ 

        try {
           const periodo = await Promise.all([
                this._obterNegociacoesDaSemana(),
                this._obterNegociacoesDaSemanaAnterior(),
                this._obterNegociacoesDaSemanaRetrasada()
            ]);

            return periodo
            .reduce((novoArr, negociacoesArr) => novoArr.concat(negociacoesArr), [])
            .sort((arg0, arg1) =>  arg1.data.getTime() - arg0.data.getTime());

        }catch(err){ 
            console.error(err);
            throw new ApplicationException('Não foi possível obter as negociações do perído');
        }
    }

    _buscaNegociacoes(url) {
        return this._http.get(url)
        .then(data => {
          return data.map(obj => new Negociacao({
              "_data" : new Date(obj.data),
              "_quantidade" : obj.quantidade,
              "_valor" : obj.valor
          }));
        }, err => {
            console.error(err);
            return err;
        });
    }
}