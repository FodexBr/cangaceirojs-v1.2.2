export class HttpService {

    /* API FETCH */
    get(url) {

        return fetch(url)
            .then(res => this._handleError(res))
            .then(res => res.json());
    }


    /* Promise 
    get(url) {
        return new Promise((resolve, reject) => {
            this._ajax(
                {"method" : "GET","url" : url}, 
                (err, data) => err != null ? reject(err) : resolve(JSON.parse(data)));
        });
    }
    */

    async post(url, dados) {

        const headers = new Headers();
        headers.set("Content-Type", "application/json");

       return await fetch(url, {
            "method" : "POST",
            "headers" : headers,
            "body": JSON.stringify(dados)
        }).then((res) => this._handleError(res));
    }

    _handleError(res) {
        if(!res.ok) throw new Error(res.statusText);
        return res;
    }

    _ajax(obj = {
        "method" : "GET",
        "url" : "",
        "Content-Type" : "application/json",
        "data" : null
    }, callback) {

        //Objeto JS XMLHttpRequest, para realziar requisicoes ajax
        const xhr = new XMLHttpRequest();

        // Open , recebe o verbo HTTP + Endereço do Servidor
        xhr.open(obj.method, obj.url);

        // Listener 'onreadystatechange ' que capitura o Status da Requisicao
        xhr.onreadystatechange = () => {

        /*  
            Estados possíveis da Requisicao
            0:	requisição	ainda	não	iniciada;
            1:	conexão	com	o	servidor	estabelecida;
            2:	requisição	recebida;
            3:	processando	requisição;
            4:	requisição	está	concluída	e	a	resposta	está	pronta 
        */
            if(xhr.readyState == 4) {
                if(xhr.status == 200) {
                    callback(null, xhr.responseText);
                }else {
                    callback(xhr.responseText);
                }
            }
        }
        obj.data != null ? xhr.send(JSON.stringify(obj.data)) : xhr.send();
        
    }  
    
}