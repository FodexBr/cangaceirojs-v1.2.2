import { ProxyFactory } from './ProxyFactory.js';

export class Bind {
    constructor(model, view, ...props) {

        const proxy = ProxyFactory.create(model, props, modelo => view.update(modelo));
        view.update(model);

        return proxy;

    }
}