export function debounce(callback, timemilis) {
    let timer = 0;
    return () => {
        clearTimeout(timer);
        timer = setTimeout(() => callback(), timemilis);
    }
}