
    let connection = null;
    let copyClose = null;

    const database = "jscangaceiro";
    const dbVersion = 2;
    const stores = ['negociacoes'];
    
   export class ConnectionFactory {
    
        constructor() {
            throw new Error('ConnectionFactory não pode ser instanciada');
        }
    
        static getConnection() {
            return new Promise((resolve, reject) => {
    
                if(connection) return resolve(connection);
    
                const openRequest = indexedDB.open(database, dbVersion);
    
                openRequest.onupgradeneeded = event => {
                    ConnectionFactory._createStores(event.target.result);
                }
    
                openRequest.onsuccess = event => {
                    connection = event.target.result;
                    copyClose = connection.close.bind(connection);

                    connection.close = () => {
                        throw new Error('A conexão só pode ser fechada através de ConnectionFactory.closeConnection()');
                    }

                    resolve(event.target.result);
                }
    
                openRequest.onerror = event => {
                    reject(event.target.error);
                }
            });
        }
    
       static _createStores(connection) {
            stores.forEach(store => {
                if(connection.objectStoreNames.contains(store)) {
                    connection.deleteObjectStore(store);
                }
                connection.createObjectStore(store, {autoIncrement: true});
            });
        }
    
        static closeConnection() {
            if(connection) {
               copyClose();
            }
        }

    }
