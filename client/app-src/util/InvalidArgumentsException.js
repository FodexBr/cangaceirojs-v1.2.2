import { ApplicationException } from './ApplicationException.js';
export class InvalidArgumentsExcepion extends ApplicationException {

    constructor(msg) {
        super(msg);
    }

}