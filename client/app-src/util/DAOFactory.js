import { ConnectionFactory } from './ConnectionFactory.js';
import { NegociacaoDAO } from '../domain/negociacao/NegociacaoDAO.js';

export async function getNegociacaoDao() {
    let con = await ConnectionFactory.getConnection();
    return new NegociacaoDAO(con);
}