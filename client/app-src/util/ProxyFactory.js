export class ProxyFactory {

    static create(instanceTarget, methodsInteceptors, callback) {

        return new Proxy(instanceTarget, {
            get(target, prop, receiver) {
                if(ProxyFactory._isIncludes(methodsInteceptors, prop) && ProxyFactory._ehFuncao(target[prop])) {
                    return function() {
                        console.log(`"${prop}" disparou a armadilha`);

                        // chamando o método original de Negociacoes dentro do proxy , passando o this(target) de negociacoes, e os argumentos(arguments)
                        target[prop].apply(target, arguments);  // call(target, ...arguments);
                        callback(target);
                    }
                }else {
                    return target[prop];
                }
            },

            set(target, prop, value, receiver) {

                const updated = Reflect.set(target, prop, value);

                if(ProxyFactory._isIncludes(methodsInteceptors, prop)) {
                    callback(target);
                }

                return updated;

            }

        });


    }


    static _ehFuncao(func) {
        return typeof(func) == typeof(Function);
    }

    static _isIncludes(arr, comparavel) {
        return arr.includes(comparavel);
    }

}