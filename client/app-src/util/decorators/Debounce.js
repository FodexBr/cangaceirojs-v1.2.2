/*Criando um Decorator */
export function debounce(milisegundos = 500) {

    return function(target, key, descriptor ) {

        /*
            "target": alvo do decorator
            "key" : nome da propriedade ou método que o decorator foi utilizando
            "descritptor":é um objeto que atraves de "descriptr.value",
            guarda a implementacao original do método ou funcao

            OBS : sempre devemos retornar o objeto descriptor 
        */

        const metodoOriginal = descriptor.value;

        let timer = 0;
        
        // a funcao de sobrescrita do método original deve receber os parametros
        descriptor.value = function(...args) {
            if(event) event.preventDefault();

            clearTimeout(timer);
            timer = setTimeout(() => metodoOriginal.apply(this, args), milisegundos);
        }


        return descriptor;
    }
}