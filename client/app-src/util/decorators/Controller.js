export function controller(...seletores) {
    
    //Ira pegar os seletores da anotacao: constroller("#selector1", "#selector2" ...)
    const elementos = seletores.map(seletor => document.querySelector(seletor)); 
    
    //Passar os elemento do dom no array "elemento" para o construtor ele
    return function(constructor) {

        const constructorOriginal = constructor; // costrutor original do NegociacaoController
        const constructorNovo = function() // Criar um novo constructor
        {    
            // instancia o cosntrutor original e passa o array de elementos, utilizando o spread-operator
            const instance = new constructorOriginal(...elementos); 
            
            /*
            Daqui em diante vamos associar oque passamos da anotacao @bindEvent 
            que é o 'event', 'selector', 
            vamos pegar esse dados , obtendo pelo Reflect.getMetadata

            vamos verificar quais propriedades ou métodos do constroller NegociacoesController
            possuem um metadado 'bindEvent'

            OBS:  essa anotacao (@controller) entra em acao
            qaundo instanciarmos pela primeira vez o NegociacaoController

            */

            // verificar quais propriedades da nossa insatncia de negociacaoController tem
            // o metadado 'bindEvent' associado
            Object.getOwnPropertyNames(constructorOriginal.prototype)
            .forEach(property => {
                if(Reflect.hasMetadata('bindEvent', instance, property)) { // verifica qauis a propriedade sou metodos tem o bindEvent
                    //associar o metodo a um evento
                    associaEvento(instance, Reflect.getMetadata('bindEvent', instance, property));// se tiver associa ao evento 
                }
            });
        }
 
        // ajustando o prototype do novo construtor
        constructorNovo.prototype = constructorOriginal.prototype;
        return constructorNovo;
    }
}
            
function associaEvento(instance, metadado) {
    
    document
    .querySelector(metadado.selector) 
    .addEventListener(metadado.event, event => {		
        if(metadado.prevent) event.preventDefault(); 
        instance[metadado.propertyKey](event);
    });  
}