import { obrigatorio } from '../Obrigatorio.js';

export function bindEvent(
    event = obrigatorio('event'),
    selector = obrigatorio('selector'),
    prevent = true) 
{
    return function(target, propertyKey, descriptor) 
    {
        // target: alvo do decorator
        // propertyKey: é o nome do método, propriedade ou funcao 
        // descriptor: é um objeto que grada a implementacao do método ou funcao, ou valor da property

        // event = evento de 'click', 'onmouseup' ...
        // selector = seletor css : #data, #quantidade, .valor ...
        // prevent = se queremos o event.preventDefault()

        /*
        Definir um "metadado" para podermos marcar qual 
        método ou propriedade utilizou a anotacao: 
        @bindEvent('click', '#seletor'), por exemplo. 
        */

        /*  
            OBS: um metado deve ser colocado em uma classe , especificamente
            em seus metodos, pois a classe terá de ser instanciada

            Sintaxe para criar um metadado:
            Reflect.defineMetadata(
                'nameOfMetadata', 
                {
                    "event": event,
                    "selector" : selector,
                    "prevent": true or false,
                    "propertyKey": propertyKey
                },
                Object.getPrototypeOf(target) // pegar o prototype do alvo do decorator
                propertyKey// passa o nome do método ou propriedade novamente
            )
        */
        console.log("PropertyKey" + propertyKey);
        Reflect.defineMetadata('bindEvent', 
            {event, selector, prevent, propertyKey},
            Object.getPrototypeOf(target),
            propertyKey);
            
            return descriptor;
    }
}