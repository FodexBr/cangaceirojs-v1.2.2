import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap/dist/css/bootstrap-theme.css';
import 'jquery/dist/jquery.js';
import 'bootstrap/js/modal.js'

import '../css/meucss.css';

import { NegociacaoController } from './controllers/NegociacaoController.js';
import { Negociacao } from './domain/index.js';

const controller = new NegociacaoController(); // aqui o @controller entra em acao e verifica quem tem metadata 'bindEvent'


// Faz o POST coma API FETCH
const negociacao = new Negociacao({
    "_data": new Date(),
    "_quantidade" : 1,
    "_valor": 120.89
});

const headers = new Headers();
headers.set('Content-Type', 'application/json');

const config = {
    "method" :"POST",
    "headers" : headers,
    "body": JSON.stringify(negociacao)
};

fetch('http://localhost:3000/negociacoes', config).then(() => console.log("Negociação enviada com sucesso!"));

jQuery("h1").click(() => $(this).modal);



 // Generators ou Funcoes Geradoras

 function * minhaFuncaoGeradora() {
    
                for(let i =1; i < 4; i ++) {
    
                    yield i; // yield , ele para a execução do 'for' e retorna o valor de I
                }
            }
    
            const genetator = minhaFuncaoGeradora();
    
            /*next,  Devolve um objeto que contem o valor retornado por "yield" e um boolean, 
            o boolean retornado diz se o bloco da funcao geradora chegou ao fim*/
            let obj = null;
            while(!(obj = genetator.next()).done) {// enquanto o done for false, ou seja, enquanto o codigo da minah funcao geradora nao terminou
                console.log(obj.value);
                // console.log(obj.value); // valor retornando por yield
                //console.log(obj.done);  // o codigo jah foi finalizado? true or false(no nosso caso false, porq o for ainda nao chegou ao fim)
            }
    