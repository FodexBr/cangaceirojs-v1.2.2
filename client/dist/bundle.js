webpackJsonp([1],[
/* 0 */,
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Negociacao; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__util_Obrigatorio_js__ = __webpack_require__(7);



let Negociacao = class Negociacao {

    constructor(args = {
        "_data": __WEBPACK_IMPORTED_MODULE_0__util_Obrigatorio_js__["a" /* obrigatorio */]('data'),
        "_quantidade": __WEBPACK_IMPORTED_MODULE_0__util_Obrigatorio_js__["a" /* obrigatorio */]('quantidade'),
        "_valor": __WEBPACK_IMPORTED_MODULE_0__util_Obrigatorio_js__["a" /* obrigatorio */]('valor')
    }) {
        let keys = Object.keys(args);

        keys.forEach((key, index) => {
            if (!key.startsWith("_")) throw new Error(`A prop "${key}" deve começar com "_"(underline)`);

            if (args[key] instanceof Date) args[key] = new Date(args[key]);
        });

        Object.assign(this, args);
        Object.freeze(this);
    }

    get data() {
        return new Date(this._data.getTime());
    }

    get quantidade() {
        return this._quantidade;
    }

    get valor() {
        return this._valor;
    }

    get volume() {
        return this._quantidade * this._valor;
    }

    equals(negociacao) {
        return JSON.stringify(this) == JSON.stringify(negociacao);
    }
};

/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ApplicationException; });
/* unused harmony export isApplicationException */
/* harmony export (immutable) */ __webpack_exports__["b"] = getExceptionMessage;
let ApplicationException = class ApplicationException extends Error {
    constructor(msg = '') {
        super(msg);
        this.name = this.constructor.name;
    }
};

//	hack	do	System.js	para	que	a	função	tenha	acesso	à	definição	d
// a	classe

const exception = ApplicationException;
function isApplicationException(err) {

    console.log(Object.prototype(err));

    return err instanceof exception || Object.prototype(err) instanceof exception;
}

function getExceptionMessage(err) {
    if (isApplicationException(err)) {
        return err.message;
    } else {
        console.log(err);
        return 'Não	foi	possível	realizar	a	operação.';
    }
}

/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export InvalidArgumentsExcepion */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ApplicationException_js__ = __webpack_require__(2);

let InvalidArgumentsExcepion = class InvalidArgumentsExcepion extends __WEBPACK_IMPORTED_MODULE_0__ApplicationException_js__["a" /* ApplicationException */] {

    constructor(msg) {
        super(msg);
    }

};

/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return View; });
let View = class View {

    constructor(elemento) {
        this._elemento = elemento;
    }

    template(model) {
        throw new Error('O método template deve ser implementado por classes filhas!');
    }

    update(model) {
        this._elemento.innerHTML = this.template(model);
    }

};

/***/ }),
/* 5 */,
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__negociacao_Negociacao_js__ = __webpack_require__(1);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__negociacao_Negociacao_js__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__negociacao_NegociacaoDao_js__ = __webpack_require__(18);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__negociacao_Negociacoes_js__ = __webpack_require__(19);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_2__negociacao_Negociacoes_js__["a"]; });


// Remover para afzer o CodeSpliting export	*	from	'./negociacao/NegociacaoService.js';


/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = obrigatorio;
function obrigatorio(param) {
    throw new Error(`O valor de ${param} é obrogatório`);
}

/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DateConverter; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__DataInvalidaException_js__ = __webpack_require__(9);

let DateConverter = class DateConverter {

    constructor() {
        throw new Error('Essa classe não pode ser intanciada pelo operador \"new\"');
    }

    static paraData(dataTexto) {
        //                      /\d{2}\/\d{2}\/\d{4}/
        let regExp = new RegExp(/\d{2}\/\d{2}\/\d{4}/);
        if (!regExp.test(dataTexto)) {
            throw new __WEBPACK_IMPORTED_MODULE_0__DataInvalidaException_js__["a" /* DataInvalidaException */]();
        }
        return new Date(...dataTexto.split("/").reverse().map((item, index) => item - index % 2));
    }

    static paraTexto(data) {
        if (!data instanceof Date) {
            throw new Error('Insira um objeto do tipo \'new Date\' ');
            return;
        }

        let diaData = data.getDate();
        let mesData = data.getMonth() + 1;
        let anoData = data.getFullYear();

        return `${diaData}/${mesData > 0 && mesData < 10 ? "0" + mesData : mesData}/${anoData}`.trim();
    }

    static ofPattern(pattern, data) {

        let reverse = DateConverter._isReverse(pattern);
        let separator = reverse ? DateConverter._getSeparator(pattern, 4) : DateConverter._getSeparator(pattern, 2);

        let dataArr = [data.getFullYear(), data.getMonth(), data.getDate()];
        return reverse ? DateConverter._mountDateFormat(dataArr, separator) : DateConverter._mountDateFormat(dataArr.reverse(), separator);
    }

    /**
     * Métodos do POJO
     */

    static _mountDateFormat(arr, separator) {
        if (DateConverter._verifyDigitsOfMonth(arr[1] + 1)) {
            arr[1] = `0${arr[1]}`;
        }
        return `${arr[0]}${separator}${arr[1] + 1}${separator}${arr[2]}`;
    }

    static _isReverse(pattern) {
        const regExp = new RegExp(/[a-z]{4}/);
        const reverseStr = pattern.substr(0, 4);

        return regExp.test(reverseStr) ? true : false;
    }

    static _getSeparator(pattern, index) {
        return pattern.substr(index, 1);
    }

    static _verifyDigitsOfMonth(month) {
        return month > 0 && month < 10;
    }

};

/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DataInvalidaException; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__util_ApplicationException_js__ = __webpack_require__(2);


let DataInvalidaException = class DataInvalidaException extends __WEBPACK_IMPORTED_MODULE_0__util_ApplicationException_js__["a" /* ApplicationException */] {

    constructor() {
        super("A data deve estar no formato dd/MM/yyyy");
    }
};

/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProxyFactory; });
let ProxyFactory = class ProxyFactory {

    static create(instanceTarget, methodsInteceptors, callback) {

        return new Proxy(instanceTarget, {
            get(target, prop, receiver) {
                if (ProxyFactory._isIncludes(methodsInteceptors, prop) && ProxyFactory._ehFuncao(target[prop])) {
                    return function () {
                        console.log(`"${prop}" disparou a armadilha`);

                        // chamando o método original de Negociacoes dentro do proxy , passando o this(target) de negociacoes, e os argumentos(arguments)
                        target[prop].apply(target, arguments); // call(target, ...arguments);
                        callback(target);
                    };
                } else {
                    return target[prop];
                }
            },

            set(target, prop, value, receiver) {

                const updated = Reflect.set(target, prop, value);

                if (ProxyFactory._isIncludes(methodsInteceptors, prop)) {
                    callback(target);
                }

                return updated;
            }

        });
    }

    static _ehFuncao(func) {
        return typeof func == typeof Function;
    }

    static _isIncludes(arr, comparavel) {
        return arr.includes(comparavel);
    }

};

/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConnectionFactory; });

let connection = null;
let copyClose = null;

const database = "jscangaceiro";
const dbVersion = 2;
const stores = ['negociacoes'];

let ConnectionFactory = class ConnectionFactory {

    constructor() {
        throw new Error('ConnectionFactory não pode ser instanciada');
    }

    static getConnection() {
        return new Promise((resolve, reject) => {

            if (connection) return resolve(connection);

            const openRequest = indexedDB.open(database, dbVersion);

            openRequest.onupgradeneeded = event => {
                ConnectionFactory._createStores(event.target.result);
            };

            openRequest.onsuccess = event => {
                connection = event.target.result;
                copyClose = connection.close.bind(connection);

                connection.close = () => {
                    throw new Error('A conexão só pode ser fechada através de ConnectionFactory.closeConnection()');
                };

                resolve(event.target.result);
            };

            openRequest.onerror = event => {
                reject(event.target.error);
            };
        });
    }

    static _createStores(connection) {
        stores.forEach(store => {
            if (connection.objectStoreNames.contains(store)) {
                connection.deleteObjectStore(store);
            }
            connection.createObjectStore(store, { autoIncrement: true });
        });
    }

    static closeConnection() {
        if (connection) {
            copyClose();
        }
    }

};

/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HttpService; });
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

let HttpService = class HttpService {

    /* API FETCH */
    get(url) {

        return fetch(url).then(res => this._handleError(res)).then(res => res.json());
    }

    /* Promise 
    get(url) {
        return new Promise((resolve, reject) => {
            this._ajax(
                {"method" : "GET","url" : url}, 
                (err, data) => err != null ? reject(err) : resolve(JSON.parse(data)));
        });
    }
    */

    post(url, dados) {
        var _this = this;

        return _asyncToGenerator(function* () {

            const headers = new Headers();
            headers.set("Content-Type", "application/json");

            return yield fetch(url, {
                "method": "POST",
                "headers": headers,
                "body": JSON.stringify(dados)
            }).then(function (res) {
                return _this._handleError(res);
            });
        })();
    }

    _handleError(res) {
        if (!res.ok) throw new Error(res.statusText);
        return res;
    }

    _ajax(obj = {
        "method": "GET",
        "url": "",
        "Content-Type": "application/json",
        "data": null
    }, callback) {

        //Objeto JS XMLHttpRequest, para realziar requisicoes ajax
        const xhr = new XMLHttpRequest();

        // Open , recebe o verbo HTTP + Endereço do Servidor
        xhr.open(obj.method, obj.url);

        // Listener 'onreadystatechange ' que capitura o Status da Requisicao
        xhr.onreadystatechange = () => {

            /*  
                Estados possíveis da Requisicao
                0:	requisição	ainda	não	iniciada;
                1:	conexão	com	o	servidor	estabelecida;
                2:	requisição	recebida;
                3:	processando	requisição;
                4:	requisição	está	concluída	e	a	resposta	está	pronta 
            */
            if (xhr.readyState == 4) {
                if (xhr.status == 200) {
                    callback(null, xhr.responseText);
                } else {
                    callback(xhr.responseText);
                }
            }
        };
        obj.data != null ? xhr.send(JSON.stringify(obj.data)) : xhr.send();
    }

};

/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* WEBPACK VAR INJECTION */(function(jQuery, $) {/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_bootstrap_dist_css_bootstrap_css__ = __webpack_require__(14);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_bootstrap_dist_css_bootstrap_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_bootstrap_dist_css_bootstrap_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_bootstrap_dist_css_bootstrap_theme_css__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_bootstrap_dist_css_bootstrap_theme_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_bootstrap_dist_css_bootstrap_theme_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_jquery_dist_jquery_js__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_jquery_dist_jquery_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_jquery_dist_jquery_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_bootstrap_js_modal_js__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_bootstrap_js_modal_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_bootstrap_js_modal_js__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__css_meucss_css__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__css_meucss_css___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4__css_meucss_css__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__controllers_NegociacaoController_js__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__domain_index_js__ = __webpack_require__(6);
var _this = this;











const controller = new __WEBPACK_IMPORTED_MODULE_5__controllers_NegociacaoController_js__["a" /* NegociacaoController */](); // aqui o @controller entra em acao e verifica quem tem metadata 'bindEvent'


// Faz o POST coma API FETCH
const negociacao = new __WEBPACK_IMPORTED_MODULE_6__domain_index_js__["a" /* Negociacao */]({
    "_data": new Date(),
    "_quantidade": 1,
    "_valor": 120.89
});

const headers = new Headers();
headers.set('Content-Type', 'application/json');

const config = {
    "method": "POST",
    "headers": headers,
    "body": JSON.stringify(negociacao)
};

fetch('http://localhost:3000/negociacoes', config).then(() => console.log("Negociação enviada com sucesso!"));

jQuery("h1").click(() => $(_this).modal);

// Generators ou Funcoes Geradoras

function* minhaFuncaoGeradora() {

    for (let i = 1; i < 4; i++) {

        yield i; // yield , ele para a execução do 'for' e retorna o valor de I
    }
}

const genetator = minhaFuncaoGeradora();

/*next,  Devolve um objeto que contem o valor retornado por "yield" e um boolean, 
o boolean retornado diz se o bloco da funcao geradora chegou ao fim*/
let obj = null;
while (!(obj = genetator.next()).done) {
    // enquanto o done for false, ou seja, enquanto o codigo da minah funcao geradora nao terminou
    console.log(obj.value);
    // console.log(obj.value); // valor retornando por yield
    //console.log(obj.done);  // o codigo jah foi finalizado? true or false(no nosso caso false, porq o for ainda nao chegou ao fim)
}
/* WEBPACK VAR INJECTION */}.call(__webpack_exports__, __webpack_require__(0), __webpack_require__(0)))

/***/ }),
/* 14 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 15 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 16 */
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NegociacaoController; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__domain_index_js__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ui_index_js__ = __webpack_require__(20);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__util_index_js__ = __webpack_require__(24);
var _dec, _dec2, _dec3, _dec4, _dec5, _class, _desc, _value, _class2;

function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }

function _applyDecoratedDescriptor(target, property, decorators, descriptor, context) {
    var desc = {};
    Object['ke' + 'ys'](descriptor).forEach(function (key) {
        desc[key] = descriptor[key];
    });
    desc.enumerable = !!desc.enumerable;
    desc.configurable = !!desc.configurable;

    if ('value' in desc || desc.initializer) {
        desc.writable = true;
    }

    desc = decorators.slice().reverse().reduce(function (desc, decorator) {
        return decorator(target, property, desc) || desc;
    }, desc);

    if (context && desc.initializer !== void 0) {
        desc.value = desc.initializer ? desc.initializer.call(context) : void 0;
        desc.initializer = undefined;
    }

    if (desc.initializer === void 0) {
        Object['define' + 'Property'](target, property, desc);
        desc = null;
    }

    return desc;
}






let NegociacaoController = (_dec = __WEBPACK_IMPORTED_MODULE_2__util_index_js__["c" /* controller */]("#data", "#quantidade", "#valor"), _dec2 = __WEBPACK_IMPORTED_MODULE_2__util_index_js__["b" /* bindEvent */]('click', '#botao-importa'), _dec3 = __WEBPACK_IMPORTED_MODULE_2__util_index_js__["d" /* debounce */](), _dec4 = __WEBPACK_IMPORTED_MODULE_2__util_index_js__["b" /* bindEvent */]('submit', '.form'), _dec5 = __WEBPACK_IMPORTED_MODULE_2__util_index_js__["b" /* bindEvent */]('click', '#botao-apaga'), _dec(_class = (_class2 = class NegociacaoController {
    constructor(_inputData, _inputQuantidade, _inputValor) {

        const self = this;
        const $ = document.querySelector.bind(document);

        this._inputData = _inputData;
        this._inputData.value = __WEBPACK_IMPORTED_MODULE_1__ui_index_js__["b" /* DateConverter */].paraTexto(new Date());

        this._inputQuantidade = _inputQuantidade;
        this._inputValor = _inputValor;

        // this._negociacoesService = new NegociacaoService();
        this._negociacoes = new __WEBPACK_IMPORTED_MODULE_2__util_index_js__["a" /* Bind */](new __WEBPACK_IMPORTED_MODULE_0__domain_index_js__["b" /* Negociacoes */](), new __WEBPACK_IMPORTED_MODULE_1__ui_index_js__["e" /* NegociacoesView */]($("#negociacoes")), 'adiciona', 'esvazia');

        this._mensagem = new __WEBPACK_IMPORTED_MODULE_2__util_index_js__["a" /* Bind */](new __WEBPACK_IMPORTED_MODULE_1__ui_index_js__["c" /* Mensagem */](), new __WEBPACK_IMPORTED_MODULE_1__ui_index_js__["d" /* MensagemView */]($("#mensagemView")), 'texto');

        this._init();
    }

    // codigos assincronos com async/await
    // OBS: o método deve comevar com "async"
    // Dentro do método usamos "await" em algum método que retorna uma promise

    _init() {
        var _this = this;

        return _asyncToGenerator(function* () {

            try {
                const dao = yield __WEBPACK_IMPORTED_MODULE_2__util_index_js__["f" /* getNegociacaoDao */]();
                const negociacoes = yield dao.listaTodos();

                negociacoes.forEach(function (n) {
                    return _this._negociacoes.adiciona(n);
                });
                _this._mensagem.texto = 'As negociações foram buscadas com sucesso!';
            } catch (err) {
                _this._mensagem.texto = __WEBPACK_IMPORTED_MODULE_2__util_index_js__["e" /* getExceptionMessage */](err);
            }
        })();
    }

    _initInputs() {
        this._inputData.value = __WEBPACK_IMPORTED_MODULE_1__ui_index_js__["b" /* DateConverter */].paraTexto(new Date());
    }

    importaNegociacoes() {
        var _this2 = this;

        return _asyncToGenerator(function* () {
            try {
                const { NegociacaoService } = yield __webpack_require__.e/* import() */(0).then(__webpack_require__.bind(null, 47));
                const service = new NegociacaoService();

                let negociacoes = yield service.obterNegociacoesDoPeriodo();
                negociacoes = negociacoes.filter(function (novaNegociacao) {
                    return !_this2._negociacoes.negociacoes.some(function (negociacaoExistente) {
                        return novaNegociacao.equals(negociacaoExistente);
                    });
                });

                negociacoes.forEach(function (negociacao) {
                    return _this2._negociacoes.adiciona(negociacao);
                });

                if (negociacoes.length > 0) _this2._mensagem.texto = 'Negociações do período importadas com sucesso!';else _this2._mensagem.texto = 'As negociações do período já foram importadas!';
            } catch (err) {
                _this2._mensagem.texto = __WEBPACK_IMPORTED_MODULE_2__util_index_js__["e" /* getExceptionMessage */](err);
            }
        })();
    }

    adiciona(event) {
        var _this3 = this;

        return _asyncToGenerator(function* () {

            try {
                event.preventDefault();
                const negociacao = _this3._criaNegociacao();

                const dao = yield __WEBPACK_IMPORTED_MODULE_2__util_index_js__["f" /* getNegociacaoDao */]();
                yield dao.adiciona(negociacao);

                _this3._negociacoes.adiciona(negociacao);
                _this3._mensagem.texto = 'Negociação adicionada com sucesso!';
                _this3._limpar(event.target);
            } catch (err) {
                console.log(err);
                console.log(err.stack);

                if (err instanceof __WEBPACK_IMPORTED_MODULE_1__ui_index_js__["a" /* DataInvalidaException */]) _this3._mensagem.texto = __WEBPACK_IMPORTED_MODULE_2__util_index_js__["e" /* getExceptionMessage */](err);else _this3._mensagem.texto = __WEBPACK_IMPORTED_MODULE_2__util_index_js__["e" /* getExceptionMessage */](err);
            }
        })();
    }

    apaga() {
        var _this4 = this;

        return _asyncToGenerator(function* () {

            try {
                const dao = yield __WEBPACK_IMPORTED_MODULE_2__util_index_js__["f" /* getNegociacaoDao */]();
                yield dao.apaga();

                _this4._negociacoes.esvazia();
                _this4._mensagem.texto = 'As negociações foram apagadas com sucesso!';
            } catch (err) {
                _this4._mensagem.texto = __WEBPACK_IMPORTED_MODULE_2__util_index_js__["e" /* getExceptionMessage */](err);
            }
        })();
    }

    _criaNegociacao() {
        //let $ = document.querySelector.bind(document);

        return new __WEBPACK_IMPORTED_MODULE_0__domain_index_js__["a" /* Negociacao */]({
            "_data": __WEBPACK_IMPORTED_MODULE_1__ui_index_js__["b" /* DateConverter */].paraData(this._inputData.value),
            "_quantidade": parseInt(this._inputQuantidade.value),
            "_valor": parseFloat(this._inputValor.value)
        });
    }

    _limpar(target) {
        target.reset();
        this._inputQuantidade.value = 1;
        this._inputValor.value = 0.0;

        this._inputData.focus();
        this._initInputs();
    }
}, (_applyDecoratedDescriptor(_class2.prototype, 'importaNegociacoes', [_dec2, _dec3], Object.getOwnPropertyDescriptor(_class2.prototype, 'importaNegociacoes'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'adiciona', [_dec4], Object.getOwnPropertyDescriptor(_class2.prototype, 'adiciona'), _class2.prototype), _applyDecoratedDescriptor(_class2.prototype, 'apaga', [_dec5], Object.getOwnPropertyDescriptor(_class2.prototype, 'apaga'), _class2.prototype)), _class2)) || _class);

/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export NegociacaoDAO */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Negociacao_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__util_InvalidArgumentsException_js__ = __webpack_require__(3);



let NegociacaoDAO = class NegociacaoDAO {

    constructor(connection) {
        this._connection = connection;
        this._store = "negociacoes";
    }

    adiciona(negociacao) {
        if (!negociacao instanceof __WEBPACK_IMPORTED_MODULE_0__Negociacao_js__["a" /* Negociacao */]) {
            throw new InvalidArgumentsException("O parâmetro passado não é uma instância de Negociacao");
        }

        return new Promise((resolve, reject) => {

            const store = this._getStoreTransactional();
            const request = store.add(negociacao);

            request.onsuccess = event => {
                resolve();
            };

            request.onerror = event => {
                console.log(event.target.error);
                reject("Não foi possível salvar a negocação");
            };
        });
    }

    listaTodos() {
        return new Promise((resolve, reject) => {
            const negociacoes = [];
            const store = this._getStoreTransactional();
            const cursor = store.openCursor();

            cursor.onsuccess = event => {
                const atual = event.target.result;

                if (atual) {
                    const obj = atual.value;
                    negociacoes.push(new __WEBPACK_IMPORTED_MODULE_0__Negociacao_js__["a" /* Negociacao */]({
                        "_data": obj._data,
                        "_quantidade": obj._quantidade,
                        "_valor": obj._valor
                    }));

                    atual.continue();
                } else {
                    resolve(negociacoes);
                }
            };

            cursor.onerror = event => {
                console.error(event.target.error);
                reject('Não foi possível buscar as negociações!');
            };
        });
    }

    apaga() {
        return new Promise((resolve, reject) => {

            const store = this._getStoreTransactional();
            const request = store.clear();

            request.onsuccess = event => resolve();
            request.onerror = event => {
                console.error(event.target.error);
                reject('Não foi possível apagar as negociaçoes cadastradas no banco.');
            };
        });
    }

    //Métodos auxiliares para obtenção de uma store transacional
    _getStoreTransactional() {
        const transaction = this._getTransaction();
        return transaction.objectStore(this._store);
    }

    _getTransaction(mode = 'readwrite') {
        return this._connection.transaction([this._store], mode);
    }

};

/***/ }),
/* 19 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Negociacoes; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Negociacao_js__ = __webpack_require__(1);

let Negociacoes = class Negociacoes {

    constructor() {
        this._negociacoes = [];
        Object.freeze(this);
    }

    adiciona(negociacao) {
        this._negociacoes.push(negociacao);
    }

    esvazia() {
        this._negociacoes.length = 0;
    }

    get negociacoes() {
        return [].concat(this._negociacoes);
    }

    get total() {
        return this._negociacoes.reduce((total, n) => total + n.volume, 0.0);
    }

};

/***/ }),
/* 20 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__views_MensagemView_js__ = __webpack_require__(21);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_0__views_MensagemView_js__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__views_NegociacoesView_js__ = __webpack_require__(22);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_1__views_NegociacoesView_js__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__views_View_js__ = __webpack_require__(4);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__models_Mensagem_js__ = __webpack_require__(23);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_3__models_Mensagem_js__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__converters_DataInvalidaException_js__ = __webpack_require__(9);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_4__converters_DataInvalidaException_js__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__converters_DateConverter_js__ = __webpack_require__(8);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_5__converters_DateConverter_js__["a"]; });







/***/ }),
/* 21 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MensagemView; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__View_js__ = __webpack_require__(4);

let MensagemView = class MensagemView extends __WEBPACK_IMPORTED_MODULE_0__View_js__["a" /* View */] {

    constructor(elemento) {
        super(elemento);
    }

    template(model) {
        return model.texto.length ? `<p	class="alert alert-info">${model.texto}</p>` : '';
    }

};

/***/ }),
/* 22 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NegociacoesView; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__View_js__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__domain_negociacao_Negociacao_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__converters_DateConverter_js__ = __webpack_require__(8);




let NegociacoesView = class NegociacoesView extends __WEBPACK_IMPORTED_MODULE_0__View_js__["a" /* View */] {

    constructor(elemento) {
        super(elemento);
    }

    template(model) {
        return `
            <table class="table table-hover table-bordered">
                <thead>
                    <tr>
                        <th>DATA</th>
                        <th>QUANTIDADE</th>
                        <th>VALOR</th>
                        <th>VOLUME</th>
                    </tr>
                </thead>
                
                <tbody>
                    ${model.negociacoes.map(negociacao => `<tr>
                            <td>${__WEBPACK_IMPORTED_MODULE_2__converters_DateConverter_js__["a" /* DateConverter */].paraTexto(negociacao.data)}</td>
                            <td>${negociacao.quantidade}</td>
                            <td>${negociacao.valor}</td>
                            <td>${negociacao.volume}</td>
                        </tr>`).join('')}
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3"></td>
                        <td>${model.total}</td>
                    </tr>
                </tfoot>
            </table>    
        `;
    }

};

/***/ }),
/* 23 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Mensagem; });
let Mensagem = class Mensagem {

    constructor(texto = '') {
        this._texto = texto;
    }

    get texto() {
        return this._texto;
    }

    set texto(text) {
        this._texto = text;
    }

};

/***/ }),
/* 24 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Bind_js__ = __webpack_require__(25);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__Bind_js__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ConnectionFactory_js__ = __webpack_require__(11);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__DaoFactory_js__ = __webpack_require__(26);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "f", function() { return __WEBPACK_IMPORTED_MODULE_2__DaoFactory_js__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ApplicationException_js__ = __webpack_require__(2);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "e", function() { return __WEBPACK_IMPORTED_MODULE_3__ApplicationException_js__["b"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__HttpService_js__ = __webpack_require__(12);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ProxyFactory_js__ = __webpack_require__(10);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__InvalidArgumentsException_js__ = __webpack_require__(3);
/* unused harmony namespace reexport */
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__decorators_Debounce_js__ = __webpack_require__(28);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_7__decorators_Debounce_js__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__decorators_Controller_js__ = __webpack_require__(29);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_8__decorators_Controller_js__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__decorators_BindEvent_js__ = __webpack_require__(30);
/* harmony namespace reexport (by used) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_9__decorators_BindEvent_js__["a"]; });








// Antes do Decorator = export  *   from    './Debounce.js';

//Apos o Decorator




/***/ }),
/* 25 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Bind; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ProxyFactory_js__ = __webpack_require__(10);


let Bind = class Bind {
    constructor(model, view, ...props) {

        const proxy = __WEBPACK_IMPORTED_MODULE_0__ProxyFactory_js__["a" /* ProxyFactory */].create(model, props, modelo => view.update(modelo));
        view.update(model);

        return proxy;
    }
};

/***/ }),
/* 26 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return getNegociacaoDao; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__ConnectionFactory_js__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__domain_negociacao_NegociacaoDAO_js__ = __webpack_require__(27);
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }




let getNegociacaoDao = (() => {
    var _ref = _asyncToGenerator(function* () {
        let con = yield __WEBPACK_IMPORTED_MODULE_0__ConnectionFactory_js__["a" /* ConnectionFactory */].getConnection();
        return new __WEBPACK_IMPORTED_MODULE_1__domain_negociacao_NegociacaoDAO_js__["a" /* NegociacaoDAO */](con);
    });

    return function getNegociacaoDao() {
        return _ref.apply(this, arguments);
    };
})();

/***/ }),
/* 27 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return NegociacaoDAO; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Negociacao_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__util_InvalidArgumentsException_js__ = __webpack_require__(3);



let NegociacaoDAO = class NegociacaoDAO {

    constructor(connection) {
        this._connection = connection;
        this._store = "negociacoes";
    }

    adiciona(negociacao) {
        if (!negociacao instanceof __WEBPACK_IMPORTED_MODULE_0__Negociacao_js__["a" /* Negociacao */]) {
            throw new InvalidArgumentsException("O parâmetro passado não é uma instância de Negociacao");
        }

        return new Promise((resolve, reject) => {

            const store = this._getStoreTransactional();
            const request = store.add(negociacao);

            request.onsuccess = event => {
                resolve();
            };

            request.onerror = event => {
                console.log(event.target.error);
                reject("Não foi possível salvar a negocação");
            };
        });
    }

    listaTodos() {
        return new Promise((resolve, reject) => {
            const negociacoes = [];
            const store = this._getStoreTransactional();
            const cursor = store.openCursor();

            cursor.onsuccess = event => {
                const atual = event.target.result;

                if (atual) {
                    const obj = atual.value;
                    negociacoes.push(new __WEBPACK_IMPORTED_MODULE_0__Negociacao_js__["a" /* Negociacao */]({
                        "_data": obj._data,
                        "_quantidade": obj._quantidade,
                        "_valor": obj._valor
                    }));

                    atual.continue();
                } else {
                    resolve(negociacoes);
                }
            };

            cursor.onerror = event => {
                console.error(event.target.error);
                reject('Não foi possível buscar as negociações!');
            };
        });
    }

    apaga() {
        return new Promise((resolve, reject) => {

            const store = this._getStoreTransactional();
            const request = store.clear();

            request.onsuccess = event => resolve();
            request.onerror = event => {
                console.error(event.target.error);
                reject('Não foi possível apagar as negociaçoes cadastradas no banco.');
            };
        });
    }

    //Métodos auxiliares para obtenção de uma store transacional
    _getStoreTransactional() {
        const transaction = this._getTransaction();
        return transaction.objectStore(this._store);
    }

    _getTransaction(mode = 'readwrite') {
        return this._connection.transaction([this._store], mode);
    }

};

/***/ }),
/* 28 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = debounce;
/*Criando um Decorator */
function debounce(milisegundos = 500) {

    return function (target, key, descriptor) {

        /*
            "target": alvo do decorator
            "key" : nome da propriedade ou método que o decorator foi utilizando
            "descritptor":é um objeto que atraves de "descriptr.value",
            guarda a implementacao original do método ou funcao
              OBS : sempre devemos retornar o objeto descriptor 
        */

        const metodoOriginal = descriptor.value;

        let timer = 0;

        // a funcao de sobrescrita do método original deve receber os parametros
        descriptor.value = function (...args) {
            if (event) event.preventDefault();

            clearTimeout(timer);
            timer = setTimeout(() => metodoOriginal.apply(this, args), milisegundos);
        };

        return descriptor;
    };
}

/***/ }),
/* 29 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = controller;
function controller(...seletores) {

    //Ira pegar os seletores da anotacao: constroller("#selector1", "#selector2" ...)
    const elementos = seletores.map(seletor => document.querySelector(seletor));

    //Passar os elemento do dom no array "elemento" para o construtor ele
    return function (constructor) {

        const constructorOriginal = constructor; // costrutor original do NegociacaoController
        const constructorNovo = function () // Criar um novo constructor
        {
            // instancia o cosntrutor original e passa o array de elementos, utilizando o spread-operator
            const instance = new constructorOriginal(...elementos);

            /*
            Daqui em diante vamos associar oque passamos da anotacao @bindEvent 
            que é o 'event', 'selector', 
            vamos pegar esse dados , obtendo pelo Reflect.getMetadata
              vamos verificar quais propriedades ou métodos do constroller NegociacoesController
            possuem um metadado 'bindEvent'
              OBS:  essa anotacao (@controller) entra em acao
            qaundo instanciarmos pela primeira vez o NegociacaoController
              */

            // verificar quais propriedades da nossa insatncia de negociacaoController tem
            // o metadado 'bindEvent' associado
            Object.getOwnPropertyNames(constructorOriginal.prototype).forEach(property => {
                if (Reflect.hasMetadata('bindEvent', instance, property)) {
                    // verifica qauis a propriedade sou metodos tem o bindEvent
                    //associar o metodo a um evento
                    associaEvento(instance, Reflect.getMetadata('bindEvent', instance, property)); // se tiver associa ao evento 
                }
            });
        };

        // ajustando o prototype do novo construtor
        constructorNovo.prototype = constructorOriginal.prototype;
        return constructorNovo;
    };
}

function associaEvento(instance, metadado) {

    document.querySelector(metadado.selector).addEventListener(metadado.event, event => {
        if (metadado.prevent) event.preventDefault();
        instance[metadado.propertyKey](event);
    });
}

/***/ }),
/* 30 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (immutable) */ __webpack_exports__["a"] = bindEvent;
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__Obrigatorio_js__ = __webpack_require__(7);


function bindEvent(event = __WEBPACK_IMPORTED_MODULE_0__Obrigatorio_js__["a" /* obrigatorio */]('event'), selector = __WEBPACK_IMPORTED_MODULE_0__Obrigatorio_js__["a" /* obrigatorio */]('selector'), prevent = true) {
    return function (target, propertyKey, descriptor) {
        // target: alvo do decorator
        // propertyKey: é o nome do método, propriedade ou funcao 
        // descriptor: é um objeto que grada a implementacao do método ou funcao, ou valor da property

        // event = evento de 'click', 'onmouseup' ...
        // selector = seletor css : #data, #quantidade, .valor ...
        // prevent = se queremos o event.preventDefault()

        /*
        Definir um "metadado" para podermos marcar qual 
        método ou propriedade utilizou a anotacao: 
        @bindEvent('click', '#seletor'), por exemplo. 
        */

        /*  
            OBS: um metado deve ser colocado em uma classe , especificamente
            em seus metodos, pois a classe terá de ser instanciada
              Sintaxe para criar um metadado:
            Reflect.defineMetadata(
                'nameOfMetadata', 
                {
                    "event": event,
                    "selector" : selector,
                    "prevent": true or false,
                    "propertyKey": propertyKey
                },
                Object.getPrototypeOf(target) // pegar o prototype do alvo do decorator
                propertyKey// passa o nome do método ou propriedade novamente
            )
        */
        console.log("PropertyKey" + propertyKey);
        Reflect.defineMetadata('bindEvent', { event, selector, prevent, propertyKey }, Object.getPrototypeOf(target), propertyKey);

        return descriptor;
    };
}

/***/ })
],[13]);