webpackJsonp([0],{

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NegociacaoService", function() { return NegociacaoService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__util_HttpService_js__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__Negociacao_js__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__util_ApplicationException_js__ = __webpack_require__(2);
function _asyncToGenerator(fn) { return function () { var gen = fn.apply(this, arguments); return new Promise(function (resolve, reject) { function step(key, arg) { try { var info = gen[key](arg); var value = info.value; } catch (error) { reject(error); return; } if (info.done) { resolve(value); } else { return Promise.resolve(value).then(function (value) { step("next", value); }, function (err) { step("throw", err); }); } } return step("next"); }); }; }





let NegociacaoService = class NegociacaoService {

    constructor() {
        this._http = new __WEBPACK_IMPORTED_MODULE_0__util_HttpService_js__["a" /* HttpService */]();
    }

    _obterNegociacoesDaSemana() {
        return this._buscaNegociacoes('http://localhost:3000/negociacoes/semana').catch(err => {
            throw new __WEBPACK_IMPORTED_MODULE_2__util_ApplicationException_js__["a" /* ApplicationException */]('Não foi possível buscas as negociacoes da semana!');
        });
    }

    _obterNegociacoesDaSemanaAnterior() {
        return this._buscaNegociacoes('http://localhost:3000/negociacoes/anterior').catch(err => {
            throw new __WEBPACK_IMPORTED_MODULE_2__util_ApplicationException_js__["a" /* ApplicationException */]('Não foi possível buscas as negociacoes da semana anterior!');
        });
    }

    _obterNegociacoesDaSemanaRetrasada() {
        return this._buscaNegociacoes('http://localhost:3000/negociacoes/retrasada').catch(err => {
            throw new __WEBPACK_IMPORTED_MODULE_2__util_ApplicationException_js__["a" /* ApplicationException */]('Não foi possível buscas as negociacoes da semana retrasada!');
        });
    }

    obterNegociacoesDoPeriodo() {
        var _this = this;

        return _asyncToGenerator(function* () {
            /** Método Sort para Ordernar arrays comparando criterios (Igual ao Java)
              *  0 => iguais
              *  1 => o arg0 > arg1
              *  -1 => o arg0 < arg1
              */

            try {
                const periodo = yield Promise.all([_this._obterNegociacoesDaSemana(), _this._obterNegociacoesDaSemanaAnterior(), _this._obterNegociacoesDaSemanaRetrasada()]);

                return periodo.reduce(function (novoArr, negociacoesArr) {
                    return novoArr.concat(negociacoesArr);
                }, []).sort(function (arg0, arg1) {
                    return arg1.data.getTime() - arg0.data.getTime();
                });
            } catch (err) {
                console.error(err);
                throw new __WEBPACK_IMPORTED_MODULE_2__util_ApplicationException_js__["a" /* ApplicationException */]('Não foi possível obter as negociações do perído');
            }
        })();
    }

    _buscaNegociacoes(url) {
        return this._http.get(url).then(data => {
            return data.map(obj => new __WEBPACK_IMPORTED_MODULE_1__Negociacao_js__["a" /* Negociacao */]({
                "_data": new Date(obj.data),
                "_quantidade": obj.quantidade,
                "_valor": obj.valor
            }));
        }, err => {
            console.error(err);
            return err;
        });
    }
};

/***/ })

});