const path = require('path');

//	IMPORTOU	O	MÓDULO	DO	WEBPACK!
const	webpack	=	require('webpack');

const babiliPlugin = require("babili-webpack-plugin");
const extractTextPlugin = require("extract-text-webpack-plugin");
const optmizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const HtmlWebpackPlugin	=	require('html-webpack-plugin');

let plugins = [];

// pkugin que ira gerar nossa pagina index.html
plugins.push(new HtmlWebpackPlugin({
    hash: true,
    minify: {
        html5: true,
        collapseWhitespace: true,
        removeComments: true
    },
    filename: 'index.html',
    template: __dirname + '/main.html'
}));

//Colocando o jQuery em Scopo Global
plugins.push(new webpack.ProvidePlugin({
    $ : 'jquery/dist/jquery.js',
    jQuery : 'jquery/dist/jquery.js'
}))

// Css gerado para importarmos no index.html
plugins.push(new extractTextPlugin("styles.css"));

// Separando o Codigo da aplicacao das Bibliotecas
plugins.push(new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    filename: 'vendor.bundle.js'
}));

// Plugins somente para Producao
if(process.env.NOD_ENV == "production") {
    
    //Adicionado o Scope Hoisting, para ganho de performance
    plugins.push(new webpack.optimize.ModuleConcatenationPlugin());

    //adiciona o plugin "babili-webpack-plugin" na lista de plugins
    plugins.push(new babiliPlugin());


	plugins.push(new optimizeCSSAssetsPlugin({
        cssProcessor:require('cssnano'),
        cssProcessorOptions: {	
            discardComments: {
                removeAll:	true	
            }
        },
        canPrint:	true
    }));	
}

module.exports = {
    entry: {
        app: './app-src/app.js',
        vendor:	['jquery', 'bootstrap', 'reflect-metadata']
    },
    output: {
        "filename" : "bundle.js",
        "path": path.join(__dirname, "dist"),
        // remover. agora que a pagina ira ser gerada "publicPath" : "dist"
    },
    "module": {
        "rules": [
            {
                "test": /\.js$/,
                "exclude": "/node_modules/",
                "use": {
                    "loader": "babel-loader"
                }
            }, 
            {
                test: /\.css$/,
                use: extractTextPlugin.extract({
                    fallback :"style-loader",
                    use: "css-loader"
                })
            },
            {	
                test:	/\.(woff|woff2)(\?v=\d+\.\d+\.\d+)?$/,	
                loader:	'url-loader?limit=10000&mimetype=application/font-woff'
            },
            {	
                test:	/\.ttf(\?v=\d+\.\d+\.\d+)?$/,	
                loader:	'url-loader?limit=10000&mimetype=application/octet-stream'
            },
            {	
                test:	/\.eot(\?v=\d+\.\d+\.\d+)?$/,	
                loader:	'file-loader'
            },
            {	
                test:	/\.svg(\?v=\d+\.\d+\.\d+)?$/,	
                loader:	'url-loader?limit=10000&mimetype=image/svg+xml'
            }
        ]
    },
    "plugins" : plugins // coloca os plugins para o webpack utilizar
};