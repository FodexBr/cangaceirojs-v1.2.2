# Separando as Bibliotecas do Codigo da aplicacao com
"CommonsChunkPlugin"

// Separando o Codigo da aplicacao das Bibliotecas
plugins.push(new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    filename: 'vendor.bundle.js'
}));

// indicar quais sao as Bibliotecas que ficaram em "vendor.bundle.js"
module.exports = {
    entry: {
        app: './app-src/app.js',
        vendor:	['jquery',	'bootstrap',	'reflect-metadata']
    }

    // ... codigo omitido
}

// agora em index.html importar esses dois scripts
<script src="dist/vendor.bundle.js"></script>
<script src="dist/bundle.js"></script>