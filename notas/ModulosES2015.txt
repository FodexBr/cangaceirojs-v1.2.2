@@ Modulos ES6(Ecma Script 2015)

@Part One
** Instalando o Loader System.js

cd client
npm init
npm install systemjs@0.20.12 --save


** Utilizando o systemjs
# Pode remover agora todos os scripts, deixando apenas esses

<script src="node_modules/systemjs/dist/syste.js"></script>
<script>
    System
    .import('app/app.js') 
    .catch(err => console.log(err));
</script>  

# Dentro de app/app.js
# O primeiro modulo que deve ser carregado, para que o SystemJS resolve as demais dependencias

import { NegociacaoController } from './controllers/NegociacaoController';
const negociacaoController =  new NegociacaoController();

# Para importar um modulo usamos: import {Modulu} from './path';
# Para exportar um modulo usamos: export class ClassName { ...  }



@Part Two
** Utilizar um transpile

# Renomear a pasta "app" para "app-src", onde ficaram os arquivos originais do projeto

# Instalar o babel(transpile)
npm install babel-cli@6.24.1 --save-dev

#Instalar o plugin para o babel que transforma nossas classes em modulos do es6
npm	install	babel-plugin-transform-es2015-modules-systemjs@6.24.1 --save-dev

# Criar o arquivo na raiz de client ".babelrc",
para explicitar que o babel deve utilizar o plugin que transforma em modulos es2015 para o systemjs
.babelrc
{
    "plugins": ["transform-es2015-modules-systemjs"]
}

# Dentro de package.json adicione a um script dentro de "scripts": {},
ele sera responsavel de dizer o diretorio das nossas classes(app-src) e falar para o babel 
transpila-los para uma pasta "app"
	"build":	"babel	app-src	-d	app	--source-maps"

# Watcher para compilar automaticamente
package.json

    "watch" : "babel app-src -d app --source-maps --watch"

@Part Tree
    ** Preset do babel para compilar codigos es217
    npm install babel-preset-es2017@6.24.1 --save-dev
    .babelrc
{
    "presets": ["es2017"],
    "plugins": ["transform-es2015-modules-systemjs"]
}