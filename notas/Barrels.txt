** Utilizando o padrao "Barrel", que serve para exportarmos
varios modulos em um unico arquivo entrategicamente, 
ex: observe a estrutura mvc

    app/
    app/models
         # criamos um index.js

    app/util
      # criamos um index.js

    app/controllers/
      # criamos um index.js

    app/ui/views
      # criamos um index.js

#Criamos um index.js para cada pasta, pois eles exportarm 
todos os modulos da pasta

    @app/models/index.js
    export * from './Model1.js';
    export * from './Model2.js';
    export * from './Model3.js';


# Na hora de importar basta importar assim:
    import { Model1, Model3, ...} from '../models/index.js';