** Utilizando Decorators(ANOTACOEs) em nossa aplicacao JS
@Part One
    #instalar plugin do babel
     npm install babel-plugin-transform-decorators-legacy@1.3.4 --save-dev

    #babel.rc
    {
    "presets": [
        "es2017"       
    ],
    "plugins": [
        "transform-es2015-modules-systemjs",
        "transform-decorators-legacy" //novidade aqui
    ]
}

    #criar o aquivo "jsconfig.json" na pasta client/,
    para dizer ao visual studio que usaremos um recurso experimental de decorators
    {
        "compilerOptions":	{
            "experimentalDecorators":	true
        }
    }
